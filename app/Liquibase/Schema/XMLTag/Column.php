<?php

namespace App\Liquibase\Schema\XMLTag;


use App\Liquibase\AbstractRenderable;

class Column extends AbstractRenderable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var Constraints
     */
    protected $constraints;

    /**
     * @var string
     */
    protected $autoIncrement = null;

    /**
     * Column constructor.
     * @param string $name
     * @param string $type
     * @param Constraints $constraints
     */
    public function __construct(string $name, string $type, Constraints $constraints)
    {
        $this->name = strtoupper($name);
        $this->type = $type;
        $this->constraints = $constraints;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        $columnString = '<column name="' . $this->name . '" type="' . $this->type . '"';

        if ($this->autoIncrement)
            $columnString .= ' autoIncrement="' . $this->autoIncrement . '"';

        if ($this->constraints) {

            $columnString .= '>' . PHP_EOL;

            $columnString .= '        ' . $this->constraints->render() . PHP_EOL;

            $columnString .= '    </column>';
        } else {
            $columnString .= '/>';
        }

        return $columnString;
    }

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function renderNameOnly()
    {
        return '<column name="' . $this->name . '"/>';
    }

    /**
     * @param string $flagContent
     * @return void
     */
    public function enableAutoIncrementFlag($flagContent = '${autoIncrement}'): void
    {
        $this->autoIncrement = $flagContent;
    }

}