<?php

namespace App\Liquibase;

abstract class AbstractValue extends AbstractRenderable
{
    /**
     * @var string
     */
    protected $value;

    /**
     * Compose the column
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

}