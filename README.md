# INSTALL

$ composer install

# Useful built in commands

php jenna tinker // this will give you a REPL command line tool to test code

php jenna app:build jenna // this will create a standalone executable that you can distribute

# DOCS for further development

https://laravel-zero.com/#/usage