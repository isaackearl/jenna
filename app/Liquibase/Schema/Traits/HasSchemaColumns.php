<?php

namespace App\Liquibase\Schema\Traits;


use App\Liquibase\Schema\XMLTag\Column;
use Illuminate\Support\Collection;

trait HasSchemaColumns
{

    /**
     * @var Collection|Column[]
     */
    protected $columns;

    /**
     * @return Column[]|Collection
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceColumns(string $stub): string
    {
        $columnsString = '';

        foreach ($this->columns as $index => $column) {

            if ($index != 0) $columnsString .= "    "; // if it is not the first

            $columnsString .= $column->render();

            if ($index != count($this->columns) - 1) $columnsString .= PHP_EOL; // if it is not the last
        }


        return str_replace('{{columns}}', $columnsString, $stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceColumnsNameOnly(string $stub): string
    {
        $columnsString = '';

        foreach ($this->columns as $index => $column) {

            if ($index != 0) $columnsString .= "    "; // if it is not the first

            $columnsString .= $column->renderNameOnly();

            if ($index != count($this->columns) - 1) $columnsString .= PHP_EOL; // if it is not the last
        }

        return str_replace('{{columns}}', $columnsString, $stub);
    }
}