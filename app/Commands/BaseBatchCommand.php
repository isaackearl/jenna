<?php

namespace App\Commands;


use App\Liquibase\ChangeSet;
use App\Liquibase\Migration;
use Exception;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class BaseBatchCommand extends Command
{
    /**
     * @var Worksheet
     */
    protected $worksheet;
    /**
     * @var Spreadsheet
     */
    protected $spreadSheet;

    protected function readSpreadsheet()
    {
        // tasks
        $this->loadSpreadSheet();
        $this->loadActiveWorksheet();
    }

    /**
     * Loads the spreadsheet
     */
    protected function loadSpreadSheet(): void
    {
        $this->task('Load Spreadsheet', function () {
            try {
                $file = realpath($this->argument('spreadsheet'));
                $inputFileType = IOFactory::identify($file);
                $reader = IOFactory::createReader($inputFileType);
//                $reader->setReadDataOnly(true);
                $this->spreadSheet = $reader->load($file);
                return true;
            } catch (Exception $e) {
                $this->error($e->getMessage());
                return false;
            }
        });
    }

    /**
     * assign worksheet property
     */
    protected function loadActiveWorksheet()
    {
        $this->task('Select Worksheet', function () {
            $this->worksheet = $this->spreadSheet->getActiveSheet();
            return true;
        });
    }

    /**
     * Does the work of parsing the spreadsheet and rending the file
     */
    protected function makeLiquibaseFile()
    {
        $this->task('Create Migration', function () {
            $changeSets = $this->createChangeSets();
            $migration = $this->createMigrations($changeSets);
            if (!$response = $this->writeXmlFile($migration)) {
                return false;
            }
            return true;
        });

        $this->info('Finished!');
    }

    /**
     * @param Migration $migration
     * @return int|bool
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function writeXmlFile($migration)
    {
        return $migration->write($this->option('name'));
    }

    /**
     * Parses the updates from the spreadsheet
     *
     * @return Collection|ChangeSet[]
     */
    abstract function createChangeSets();

    /**
     * @param $changeSets
     * @return Migration
     */
    public function createMigrations($changeSets): Migration
    {
        return new Migration(...$changeSets);
    }
}