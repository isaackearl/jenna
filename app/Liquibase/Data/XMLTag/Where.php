<?php

namespace App\Liquibase\Data\XMLTag;


use App\Liquibase\AbstractValue;
use App\Liquibase\Data\RawWhereClause;

class Where extends AbstractValue
{

    /**
     * Where constructor.
     * @param RawWhereClause[] $whereClauses
     */
    public function __construct(RawWhereClause ... $whereClauses)
    {
        $value = $this->parseClauses($whereClauses);

        parent::__construct($value);
    }

    /**
     * @param RawWhereClause[] $whereClauses
     * @return string
     */
    protected function parseClauses($whereClauses)
    {
        $value = '';

        foreach ($whereClauses as $index => $clause) {

            if ($index > 0)
                $value .= ' AND ';

            $value .= $clause;
        }

        return trim($value);
    }

    /**
     * @return string
     */
    function render()
    {
        return '<where>'
            . $this->getValue()
            . '</where>';
    }
}