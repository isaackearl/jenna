<?php

namespace App\Liquibase\Data\XMLTag;


use App\Liquibase\ChangeSetContent;
use App\Liquibase\Data\Traits\HasWhere;

class Delete extends ChangeSetContent
{

    use HasWhere;

    /**
     * ChangeSet constructor.
     * @param string $schema
     * @param string $table
     * @param Where $where
     */
    public function __construct($schema, $table, Where $where)
    {
        parent::__construct($schema, $table);
        $this->where = $where;
    }

    /**
     * @return string;
     */
    function getChangeSetType()
    {
        return 'delete';
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub()
    {
        return $this->files->get(app_path() . '/stubs/delete.stub');
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceValues(string $stub)
    {
        $stub = $this->replaceSchema($stub);
        $stub = $this->replaceTable($stub);
        return $this->replaceWhere($stub);
    }
}