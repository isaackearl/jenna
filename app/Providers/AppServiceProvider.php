<?php

namespace App\Providers;

use App\Liquibase\Schema\Services\Interfaces\SchemaParserInterface;
use App\Liquibase\Schema\Services\SchemaParser;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }
}
