<?php

namespace App\Liquibase\Data\XMLTag;


use App\Liquibase\ChangeSetContent;
use App\Liquibase\Data\Traits\HasDataColumns;
use App\Liquibase\Data\Traits\HasWhere;

class ConditionalInsert extends ChangeSetContent
{

    use HasDataColumns, HasWhere;

    /**
     * ChangeSet constructor.
     * @param $schema
     * @param $table
     * @param Column[] $columns
     * @param Where $where
     */
    public function __construct($schema, $table, Where $where, Column ... $columns)
    {
        parent::__construct($schema, $table);
        $this->columns = $columns;
        $this->where = $where;
    }

    /**
     * @return string;
     */
    function getChangeSetType()
    {
        return 'conditional insert';
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub()
    {
        return $this->files->get(app_path() . '/stubs/conditionalInsert.stub');
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceValues(string $stub)
    {
        $stub = $this->replaceSchema($stub);
        $stub = $this->replaceTable($stub);
        $stub = $this->replaceWhereValue($stub);
        $stub = $this->replaceColumnNames($stub);
        return $this->replaceColumnValues($stub);
    }
}