<?php

namespace App\Liquibase;

use Illuminate\Contracts\Support\Renderable;

abstract class AbstractRenderable implements Renderable
{

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

}