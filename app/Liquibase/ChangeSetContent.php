<?php

namespace App\Liquibase;


use Illuminate\Filesystem\Filesystem;

abstract class ChangeSetContent extends AbstractRenderable
{

    /**
     * @var string
     */
    protected $schema;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * ChangeSet constructor.
     * @param $schema
     * @param $table
     */
    public function __construct(string $schema, string $table)
    {
        $this->files = app(Filesystem::class);
        $this->schema = strtoupper($schema);
        $this->table = strtoupper($table);
    }

    /**
     * @return string;
     */
    abstract function getChangeSetType();

    /**
     * Alias for toString()
     *
     * @return string
     */
    public function render()
    {
        $stub = $this->getStub();
        return $this->replaceValues($stub);
    }

    /**
     * @return string
     */
    protected abstract function getStub();

    /**
     * @param string $stub
     * @return string
     */
    protected abstract function replaceValues(string $stub);

    /**
     * @return string
     */
    public function getSchema(): string
    {
        return $this->schema;
    }

    /**
     * @param string $schema
     */
    public function setSchema(string $schema): void
    {
        $this->schema = $schema;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable(string $table): void
    {
        $this->table = $table;
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceSchema($stub)
    {
        return str_replace('{{schema}}', $this->schema, $stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceTable($stub)
    {
        return str_replace('{{table}}', $this->table, $stub);
    }

}