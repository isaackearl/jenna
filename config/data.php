<?php

return [
    'fragment-tables' => [
        'FRAGMENT' => [
            'schema' => 'RTNG_FRGMT',
            'table' => 'FRAGMENT',
            'fragment-identifier' => 'FRAGMENT_RATING_CODE',
            'discriminator' => 'DISCRIMINATOR',
            'update-columns' => [
                'GENERATED_TXT',
                'SNL_TXT'
            ],
            'insert-columns' => [
                'valueComputed' => [
                    'ID' => 'RTNG_FRGMT.FRGMT_SEQ.NEXTVAL'
                ],
                'fragment' => [
                    'GENERATED_TXT',
                    'SNL_TXT'
                ],
                'discriminator' => [
                    'DISCRIMINATOR',
                ],
                'identifier' => [
                    'FRAGMENT_RATING_CODE',
                    'SNL_CODE'
                ]
            ],
        ]
    ],
    'schemas' => [ // put everything you put here in caps...
        'VASRD' => ['RULES_MANAGER'],
        'RATINGS' => ['RTNG*'], // * is wildcard.
    ]
];