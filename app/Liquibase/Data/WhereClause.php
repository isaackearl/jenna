<?php

namespace App\Liquibase\Data;


class WhereClause extends RawWhereClause
{

    /**
     * BasicWhereClause constructor.
     *
     * @param string $key
     * @param string $value
     * @param string $operator
     */
    public function __construct(string $key, string $value, string $operator = '=')
    {
        $value = $key . ' ' . $operator . ' \'' . $value . '\'';
        parent::__construct($value);
    }

}