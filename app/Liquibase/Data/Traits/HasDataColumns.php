<?php

namespace App\Liquibase\Data\Traits;


use App\Liquibase\Data\XMLTag\Column;
use Illuminate\Support\Collection;

trait HasDataColumns
{

    /**
     * @var Collection|Column[]
     */
    protected $columns;

    /**
     * @return Column[]|Collection
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param Column[]|Collection $columns
     */
    public function setColumns($columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceColumns(string $stub): string
    {
        $columnsString = '';

        foreach ($this->columns as $index => $column) {

            if ($index != 0) $columnsString .= "    "; // if it is not the first

            $columnsString .= $column->render();

            if ($index != count($this->columns) - 1) $columnsString .= PHP_EOL; // if it is not the last
        }


        return str_replace('{{columns}}', $columnsString, $stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceColumnNames(string $stub): string
    {
        $columnNames = '';

        foreach ($this->columns as $index => $column) {

            $columnNames .= $column->getName();

            if ($index != count($this->columns) - 1) $columnNames .= ', ' . PHP_EOL . '    '; // if it is not the last
        }


        return str_replace('{{columnNames}}', $columnNames, $stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceColumnValues(string $stub): string
    {
        $columnValues = '';

        foreach ($this->columns as $index => $column) {

            if ($column->getMode() == 'valueComputed')
                $columnValues .= $column->getValue();
            else
                $columnValues .= '\'' . $column->getValue() . '\'';

            if ($index != count($this->columns) - 1) $columnValues .= ', ' . PHP_EOL . '    '; // if it is not the last
        }

        return str_replace('{{columnValues}}', $columnValues, $stub);
    }

}