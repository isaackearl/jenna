<?php

namespace App\Liquibase\Data\XMLTag;


use App\Liquibase\AbstractNamedValue;

class Column extends AbstractNamedValue
{
    /**
     * @var null|string
     */
    protected $mode;

    /**
     * Compose the column
     *
     * @param string $name
     * @param string $value
     * @param string $mode
     */
    public function __construct(string $name, string $value, $mode = 'default')
    {
        parent::__construct(strtoupper($name), $value);
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function render()
    {
        switch ($this->mode) {
            case 'default':
                return $this->renderDefault();
                break;
            case 'valueComputed':
                return $this->renderComputed();
                break;
        }
    }

    protected function renderDefault()
    {
        $cdata = '<![CDATA[' . $this->getValue() . ']]>';

        return
            '<column name="' . $this->getName() . '">' .
            $cdata .
            '</column>';
    }

    protected function renderComputed()
    {
        return
            '<column name="'
            . $this->getName()
            . '" valueComputed="'
            . $this->getValue()
            . '"/>';
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

}