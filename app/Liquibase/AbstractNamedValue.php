<?php

namespace App\Liquibase;


abstract class AbstractNamedValue extends AbstractValue
{
    /**
     * @var string
     */
    protected $name;

    /**
     * Compose the column
     *
     * @param string $name
     * @param string $value
     */
    public function __construct(string $name, string $value)
    {
        $this->name = $name;
        parent::__construct($value);
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}