<?php

namespace App\Commands;

use App\Liquibase\Schema\Services\SchemaParser;
use LaravelZero\Framework\Commands\Command;

class MakeMigrationSchemaCommand extends Command
{

    protected $fields;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:migration:schema
                            {schema : The name of the schema}
                            {table : The name of the table that will be created/removed/modified}
                            {--f|fields= : Optional fields to be created with migration, separated by a comma. Format looks like this -> id:bigint:primary, person_id:number(38):foreign->person.id:unique:index, some_date:date, another_value:varchar(200):nullable}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Create a liquibase migration and generate syntax for columns/indexes/constraints etc';


    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(): void
    {
        $schemaName = $this->argument('schema');
        $tableName = $this->argument('table');
        $fields = $this->option('fields');

        $migration = (new SchemaParser($schemaName, $tableName))->parse($fields);

        $migration->write('create_' . $tableName . '_table');
    }

}
