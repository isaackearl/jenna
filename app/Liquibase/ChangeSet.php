<?php

namespace App\Liquibase;


use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

class ChangeSet extends AbstractRenderable
{

    /**
     * @var string
     */
    protected $author;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var ChangeSetContent[]
     */
    protected $changeSetContent;

    /**
     * ChangeSet constructor.
     * @param string $author
     * @param $index
     * @param ChangeSetContent[]|Collection $changeSetContent
     */
    public function __construct($author = 'CHANGE_ME', $index, ... $changeSetContent)
    {
        $this->id = $this->generateId('data_change_' . $index);
        $this->files = app(Filesystem::class);
        $this->author = $author;
        $this->changeSetContent = collect($changeSetContent);
    }

    /**
     * @param $name
     * @return string
     */
    protected function generateId($name)
    {
        return Carbon::now()->format('Ymdhi_') . snake_case($name);
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Alias for toString()
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function render()
    {
        $stub = $this->getStub();
        $stub = $this->replaceChangeSetValues($stub);

        return $stub;
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub()
    {
        return $this->files->get(app_path() . '/stubs/changeset.stub');
    }

    /**
     * @param $stub
     * @return string
     */
    protected function replaceChangeSetValues($stub)
    {
        $stub = $this->replaceAuthor($stub);
        $stub = $this->replaceId($stub);
        return $this->replaceContent($stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    private function replaceAuthor($stub)
    {
        return str_replace('{{author}}', $this->author, $stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    private function replaceId($stub)
    {
        return str_replace('{{id}}', $this->id, $stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    function replaceContent($stub)
    {
        $contentString = '';

        foreach ($this->changeSetContent as $content) {
            $contentString .= $content->render() . PHP_EOL;
        }

        return str_replace('{{content}}', $contentString, $stub);
    }

}