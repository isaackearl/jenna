<?php

namespace App\Liquibase\Data\Traits;


use App\Liquibase\Data\XMLTag\Where;

trait HasWhere
{

    /**
     * @var Where
     */
    protected $where;


    /**
     * @return Where
     */
    public function getWhere(): Where
    {
        return $this->where;
    }

    /**
     * @param Where $where
     */
    public function setWhere(Where $where): void
    {
        $this->where = $where;
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceWhere(string $stub): string
    {
        return str_replace('{{where}}', $this->where, $stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceWhereValue(string $stub): string
    {
        return str_replace('{{where.value}}', $this->where->getValue(), $stub);
    }

}