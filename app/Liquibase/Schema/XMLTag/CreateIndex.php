<?php
/**
 * Created by PhpStorm.
 * User: isaacearl
 * Date: 7/16/18
 * Time: 12:15 PM
 */

namespace App\Liquibase\Schema\XMLTag;


use App\Liquibase\ChangeSetContent;
use App\Liquibase\Schema\Traits\HasSchemaColumns;
use Illuminate\Support\Collection;

class CreateIndex extends ChangeSetContent
{

    use HasSchemaColumns;

    /**
     * @var string
     */
    private $indexName;

    /**
     * CreateTable constructor.
     * @param string $schema
     * @param string $table
     * @param Column $column
     * @param null|string $indexName
     */
    public function __construct(string $schema, string $table, Column $column, string $indexName = null)
    {
        parent::__construct($schema, $table);
        $this->columns = new Collection([$column]);
        if (!$indexName)
            $indexName = $this->generateIndexName();
        $this->indexName = $indexName;
    }

    /**
     * @return string;
     */
    function getChangeSetType()
    {
        return 'createIndex';
    }

    /**
     * @return string
     */
    protected function getStub()
    {
        return $this->files->get(app_path() . '/stubs/createIndex.stub');
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceValues(string $stub)
    {
        $stub = $this->replaceSchema($stub);
        $stub = $this->replaceTable($stub);
        $stub = $this->replaceIndexName($stub);
        return $this->replaceColumnsNameOnly($stub);
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceIndexName($stub)
    {
        return str_replace('{{indexName}}', $this->indexName, $stub);
    }

    protected function generateIndexName()
    {
        $tableNameElements = explode('_', $this->table);

        $tableNameAbbreviation = '';
        foreach ($tableNameElements as $element) {
            $tableNameAbbreviation .= substr($element, 0, 1);
        }

        return strtoupper('idx_' . $tableNameAbbreviation . '_' . $this->columns->get(0)->getName());
    }

}