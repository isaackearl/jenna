<?php
/**
 * Created by PhpStorm.
 * User: isaacearl
 * Date: 7/16/18
 * Time: 12:15 PM
 */

namespace App\Liquibase\Schema\XMLTag;


use App\Liquibase\ChangeSetContent;
use App\Liquibase\Schema\Traits\HasSchemaColumns;

class CreateTable extends ChangeSetContent
{

    use HasSchemaColumns;

    /**
     * CreateTable constructor.
     * @param string $schema
     * @param string $table
     * @param Column[] $columns
     */
    public function __construct(string $schema, string $table, Column ... $columns)
    {
        parent::__construct($schema, $table);
        $this->columns = $columns;
    }


    /**
     * @return string;
     */
    function getChangeSetType()
    {
        return 'createTable';
    }

    /**
     * @return string
     */
    protected function getStub()
    {
        return $this->files->get(app_path() . '/stubs/createTable.stub');
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceValues(string $stub)
    {
        $stub = $this->replaceSchema($stub);
        $stub = $this->replaceTable($stub);
        return $this->replaceColumns($stub);
    }
}