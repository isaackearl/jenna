<?php

namespace App\Liquibase\Schema\Services;


use App\Liquibase\Migration;
use App\Liquibase\Schema\Services\Interfaces\SchemaParserInterface;
use App\Liquibase\Schema\XMLTag\Column;
use App\Liquibase\Schema\XMLTag\Constraints;
use App\Liquibase\Schema\XMLTag\CreateIndex;
use App\Liquibase\Schema\XMLTag\CreateTable;
use Illuminate\Support\Collection;

class SchemaParser implements SchemaParserInterface
{

    /**
     * @var string
     */
    protected $schema;
    /**
     * @var string
     */
    protected $table;

    /**
     * SchemaParser constructor.
     * @param string $schema
     * @param string $table
     */
    public function __construct(string $schema, string $table)
    {
        $this->schema = strtoupper($schema);
        $this->table = strtoupper($table);
    }


    /**
     * @param null|string $columnsString
     * @return Migration
     */
    public function parse(?string $columnsString)
    {
        $columnCollection = new Collection();
        $changeSets = new Collection();
        $indexes = new Collection();

        if ($columnsString) {
            $columnStringArray = $this->splitIntoColumns($columnsString);
            foreach ($columnStringArray as $columnString) {
                $properties = $this->getColumnProperties($columnString);
                $constraints = new Constraints($properties['nullable'], $properties['isPrimary'], $properties['primaryKeyName'], $properties['foreignKeyName'], $properties['references'], $properties['isUnique']);
                $column = new Column($properties['name'], $properties['type'], $constraints);

                // special considerations for VASRD tables.
                $column = $this->handleSpecialSchemaCases($column, $properties['isPrimary']);

                if ($properties['isIndex']) {
                    $index = new CreateIndex($this->schema, $this->table, $column);
                    $indexes->push($index);
                }

                $columnCollection->push($column);
            }
        }

        $table = new CreateTable($this->schema, $this->table, ... $columnCollection);

        $changeSets->push($table);

        foreach ($indexes as $index) {
            $changeSets->push($index);
        }

        $migration = new Migration(... $changeSets);

        return $migration;
    }

    /**
     * @param string $columnsString
     * @return string[]
     */
    private function splitIntoColumns(string $columnsString)
    {
        return array_map('trim', explode(',', $columnsString));
    }

    /**
     * @param string $columnString
     * @return string[]
     */
    private function splitIntoPropertySegments(string $columnString)
    {
        return array_map('trim', explode(':', $columnString));
    }

    /**
     * @param string $columnString
     * @return array
     */
    private function getColumnProperties(string $columnString)
    {
        $properties = $this->splitIntoPropertySegments($columnString);

        // id:number(38):primary, person_id:number(38):foreign->table.id, profile_date:date, additional_issue_key:varchar(200 char)

        $name = $properties[0];
        $type = $properties[1];

        $isIndex = false;
        $nullable = false;
        $isUnique = null;
        $isPrimary = null;
        $primaryKeyName = null;
        $foreignKeyName = null;
        $references = null;

        for ($index = 2; $index < count($properties); $index++) {

            if ($properties[$index] == 'nullable')
                $nullable = true;

            if ($properties[$index] == 'index')
                $isIndex = true;

            if ($properties[$index] == 'unique')
                $isUnique = true;

            if ($properties[$index] == 'primary') {
                $isPrimary = true;
                $primaryKeyName = strtoupper($this->table . '_PK');
            }

            if (str_contains($properties[$index], 'foreign')) {
                $references = $this->getReference($properties[$index]);
                $foreignKeyName = strtoupper($this->table . '_' . $name . '_FK');
            }
        }

        return compact('name', 'type', 'nullable', 'isUnique', 'isPrimary', 'primaryKeyName', 'foreignKeyName', 'references', 'isIndex');
    }

    /**
     * @param string $property
     * @return string
     */
    private function getReference($property)
    {
        // input would be foreign->table.id
        $tableAndPropertyString = explode('->', $property)[1];
        // now I have table.id
        $tableAndPropertyArray = explode('.', $tableAndPropertyString);
        $table = $tableAndPropertyArray[0];
        $property = $tableAndPropertyArray[1];

        return strtoupper($this->schema . '.' . $table . '(' . $property . ')');
    }

    /**
     * @param Column $column
     * @param bool $isPrimary
     * @return Column
     */
    protected function handleSpecialSchemaCases($column, $isPrimary): Column
    {
        if (in_array($this->schema, config('data.schemas.VASRD')) && $isPrimary) {
            $column->enableAutoIncrementFlag();
        }

        return $column;
    }
}