<?php

namespace App\Liquibase;


use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

class Migration extends AbstractRenderable
{

    /**
     * @var Collection|ChangeSet[]
     */
    protected $changeSets;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * Migration constructor.
     * @param ChangeSet[] $changeSets
     */
    public function __construct(... $changeSets)
    {
        $this->files = app(Filesystem::class);
        $this->changeSets = $changeSets;
    }

    /**
     * @return mixed|string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getLiquibaseStub()
    {
        return $this->files->get(app_path() . '/stubs/migration.stub');
    }

    /**
     * @param $stub
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function replaceContent($stub)
    {
        $contentString = '';

        foreach ($this->changeSets as $changeSet) {

            $contentString .= $changeSet->render();

        }

        return str_replace('{{content}}', $contentString, $stub);
    }

    /**
     * @param string $name
     * @return int|bool
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function write($name = null)
    {
        if ($name == null) {
            $name = $this->getNameForFile();
        }

        $name = $this->generateId() . $name . '.xml';

        return $this->files->put($name, $this->render());
    }

    protected function getNameForFile()
    {
        return 'fragment_changes';
    }

    /**
     * @return string
     */
    protected function generateId()
    {
        return Carbon::now()->format('Ymdhi_');
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function render()
    {
        $stub = $this->getLiquibaseStub();
        $stub = $this->replaceContent($stub);

        return $stub;
    }

}