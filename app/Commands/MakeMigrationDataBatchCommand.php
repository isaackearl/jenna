<?php

namespace App\Commands;


use App\Liquibase\ChangeSet;
use App\Liquibase\Data\XMLTag\Column;
use App\Liquibase\Data\XMLTag\ConditionalInsert;
use App\Liquibase\Data\XMLTag\Insert;
use App\Liquibase\Data\XMLTag\Where;
use Illuminate\Support\Collection;

class MakeMigrationDataBatchCommand extends BaseBatchCommand
{

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:migration:data:batch
                            {spreadsheet : The name of the spreadsheet with cbr changes}
                            {schema : the name of the schema}
                            {table : the name of the table}
                            {author=CHANGE_ME : The author for the liquibase changeSets}
                            {--N|name= : Custom name for liquibase file, timestamp and extension will be added. }
                            {--vc|valueComputed= : List all the columns (by header), that should be valueComputed columns in a comma separated list ie. ID, FRAGMENT, ETC}';
//                          {--c|conditional : performs an insert only if it doesn't exist};

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Populate a table with data defined in a spreadsheet.  The columns in the spreadsheet much match the table columns';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->readSpreadsheet();
        $this->makeLiquibaseFile();
    }

    /**
     * Parses the updates from the spreadsheet
     *
     * @return Collection|ChangeSet[]
     */
    function createChangeSets()
    {
        $changeSets = new Collection();

        $this->task('Create ChangeSets', function () use (&$changeSets) {

            $headers = $this->getHeaders();

            $startingRow = 2;
            $endingRow = $this->worksheet->getHighestRow(); // this will return a number (int)

            // loop through each row
            for ($row = $startingRow; $row <= $endingRow; $row++) {

                $rowCellValues = [];
                foreach ($headers as $key => $value) {

                    $rowCellValues[$key] = $this->worksheet->getCell($key . $row)->getFormattedValue();
                }

                $changeSet = $this->buildChangeSet($headers, $rowCellValues, $row);
                $changeSets->push($changeSet);

            }

            return true; // exit closure
        });

        return $changeSets;

    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function getHeaders()
    {
        $headers = [];
        $headerRow = 1;
        $startingColumn = 'A';
        $endingColumn = $this->worksheet->getHighestColumn();
        $endingColumn++; // have to iterate it once since we loop until !=
        for ($column = $startingColumn; $column != $endingColumn; $column++) {
            $headers[$column] = $this->worksheet->getCell($column . $headerRow);
        }
        return $headers;
    }

    /**
     * @param array $headers
     * @param array $rowCellValues
     * @param int $row
     * @return ChangeSet
     */
    protected function buildChangeSet(array $headers, array $rowCellValues, $row)
    {
        $schema = $this->argument('schema');
        $table = $this->argument('table');

        $columns = $this->getColumns($headers, $rowCellValues);

//        if ($this->option('conditional')) {
//            $conditionalInsert = $this->buildConditionalInsert($meta, $columns, $where);
//            return new ChangeSet($this->argument('author'), $row, $conditionalInsert);
//        }

        $insert = $this->buildInsert($schema, $table, $columns);
        return new ChangeSet($this->argument('author'), $row, $insert);
    }

    /**
     * @param $schema
     * @param $table
     * @param Column[] $columns
     * @param Where $where
     * @return ConditionalInsert
     */
    protected function buildConditionalInsert($schema, $table, $columns, $where)
    {
        $conditionalInsert = new ConditionalInsert($schema, $table, $where, ... $columns);
        return $conditionalInsert;
    }

    /**
     * @param $schema
     * @param $table
     * @param Column[] $columns
     * @return Insert
     */
    protected function buildInsert($schema, $table, $columns)
    {
        $insert = new Insert($schema, $table, ... $columns);
        return $insert;
    }

    /**
     * @param array $headers
     * @param array $rowCellValues
     * @return Column[]|Collection
     */
    protected function getColumns(array $headers, array $rowCellValues)
    {
        $columns = new Collection();

        $valueComputed = $this->getValueComputedColumns();

        foreach ($headers as $key => $header) {

            $mode = 'default';

            if (array_search($header->getValue(), $valueComputed) !== false) {
                $mode = 'valueComputed';
            }

            if ($rowCellValues[$key]) { // only make the column if it is not null
                $column = new Column($header, $rowCellValues[$key], $mode);
                $columns->push($column);
            }
        }

        return $columns;
    }

    /**
     * @return array
     */
    protected function getValueComputedColumns()
    {
        $valueComputedString = $this->option('valueComputed');

        if ($valueComputedString) {
            return array_map('trim', explode(',', $valueComputedString));
        }

        return [];
    }
}
