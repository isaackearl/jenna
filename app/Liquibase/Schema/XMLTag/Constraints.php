<?php

namespace App\Liquibase\Schema\XMLTag;


use App\Liquibase\AbstractRenderable;
use InvalidArgumentException;

class Constraints extends AbstractRenderable
{
    /**
     * @var bool
     */
    protected $isNullable;

    /**
     * @var bool|null
     */
    protected $isUnique;

    /**
     * @var bool|null
     */
    protected $isPrimaryKey;

    /**
     * @var string|null
     */
    protected $primaryKeyName;

    /**
     * @var string|null
     */
    protected $foreignKeyName;

    /**
     * @var string|null
     */
    protected $references;

    /**
     * Constraint constructor.
     * @param bool|null $isNullable
     * @param bool|null $isPrimaryKey
     * @param string|null $primaryKeyName
     * @param string|null $foreignKeyName
     * @param string|null $references
     * @param bool|null $isUnique
     */
    public function __construct(?bool $isNullable = false, ?bool $isPrimaryKey = null, ?string $primaryKeyName = null, ?string $foreignKeyName = null, ?string $references = null, ?bool $isUnique = null)
    {
        $this->validateArgs($foreignKeyName, $references);

        if ($isPrimaryKey || $foreignKeyName)
            $isUnique = null; // we only care about this tag if it is not in combination with one of those others

        $this->isNullable = $isNullable;
        $this->isPrimaryKey = $isPrimaryKey;
        $this->primaryKeyName = $primaryKeyName;
        $this->foreignKeyName = $foreignKeyName;
        $this->references = $references;
        $this->isUnique = $isUnique;
    }


    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        $constraintString = '<constraints nullable="' . json_encode($this->isNullable) . '" ';

        if ($this->isPrimaryKey)
            $constraintString .= 'primaryKey="' . json_encode($this->isPrimaryKey) . '" ';

        if ($this->primaryKeyName)
            $constraintString .= 'primaryKeyName="' . $this->primaryKeyName . '" ';

        if ($this->foreignKeyName)
            $constraintString .= 'foreignKeyName="' . $this->foreignKeyName . '" references="' . $this->references . '" ';

        if ($this->isUnique != null)
            $constraintString .= 'unique="' . json_encode($this->isUnique) . '" ';

        return $constraintString . '/>';
    }

    /**
     * @param null|string $foreignKeyName
     * @param null|string $references
     */
    protected function validateArgs(?string $foreignKeyName, ?string $references): void
    {
        if ($foreignKeyName && !$references)
            throw new InvalidArgumentException('If this has a foreign key name, you must specify the reference');

        if ($references && !$foreignKeyName)
            throw new InvalidArgumentException('If you specify a reference table, you must name the foreign key');
    }

}