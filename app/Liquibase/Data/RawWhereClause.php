<?php

namespace App\Liquibase\Data;


use App\Liquibase\AbstractValue;

class RawWhereClause extends AbstractValue
{

    /**
     * @return string
     */
    function render()
    {
        return $this->getValue();
    }

}