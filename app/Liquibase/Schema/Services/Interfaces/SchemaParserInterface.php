<?php

namespace App\Liquibase\Schema\Services\Interfaces;


use App\Liquibase\ChangeSetContent;
use App\Liquibase\Migration;
use App\Liquibase\Schema\XMLTag\Column;
use Illuminate\Support\Collection;

interface SchemaParserInterface
{
    /**
     * @param string $columnsString
     * @return Migration
     */
    public function parse(?string $columnsString);
}