<?php

namespace App\Commands;

use App\Liquibase\ChangeSet;
use App\Liquibase\Data\WhereClause;
use App\Liquibase\Data\XMLTag\Column;
use App\Liquibase\Data\XMLTag\Delete;
use App\Liquibase\Data\XMLTag\Insert;
use App\Liquibase\Data\XMLTag\Update;
use App\Liquibase\Data\XMLTag\ConditionalInsert;
use App\Liquibase\Data\XMLTag\Where;
use Illuminate\Support\Collection;

class MakeMigrationDataFragmentCommand extends BaseBatchCommand
{

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:migration:data:fragment
                            {spreadsheet : The name of the spreadsheet with cbr changes}
                            {author=CHANGE_ME : The author for the liquibase changeSets}
                            {--N|name= : Custom name for liquibase file, timestamp and extension will be added. }
                            {--i|identifierColumn=B : Define the identifier column}
                            {--d|discriminatorColumn=A :  Define the discriminator column.}
                            {--f|fragmentColumn=C : The column with the new fragment to be inserted/updated}
                            {--a|actionColumn=D : The type of action to take on the fragment, options are: update, insert, delete, and upsert}
                            {--ignoreFirstRow : Use this if your spreadsheet has a header row that you want to skip}';
//                            {--C|chunk=500 : Number of changeSets that will be grouped per file}

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Creates a liquibase script of fragment updates from a spreadsheet';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->readSpreadsheet();
        $this->makeLiquibaseFile();
    }

    /**
     * @return array
     */
    protected function getDefaultFragmentTableInformation(): array
    {
        return config('data.fragment-tables.FRAGMENT');
    }

    /**
     * Parses the updates from the spreadsheet
     *
     * @return Collection|ChangeSet[]
     */
    public function createChangeSets()
    {
        $changeSets = new Collection();

        $this->task('Create ChangeSets', function () use (&$changeSets) {
            $startingRow = ($this->option('ignoreFirstRow')) ? 2 : 1;
            $endingRow = $this->worksheet->getHighestRow(); // this will return a number (int)

            $identifierColumn = strtoupper($this->option('identifierColumn')); // 'B' is default
            $fragmentColumn = strtoupper($this->option('fragmentColumn')); // 'C'
            $discriminatorColumn = strtoupper($this->option('discriminatorColumn')); // 'A' is default
            $actionColumn = strtoupper($this->option('actionColumn')); // 'D' is default


            // loop through each row
            for ($row = $startingRow; $row <= $endingRow; $row++) {
                $identifier = $this->worksheet->getCell($identifierColumn . $row); // I'm concatenating column and row to make something like 'A1'
                $discriminator = $this->worksheet->getCell($discriminatorColumn . $row); // this is just how this spreadsheet library works
                $newFragment = $this->worksheet->getCell($fragmentColumn . $row);
                $action = $this->worksheet->getCell($actionColumn . $row);

                if ($discriminator == "")
                    $discriminator = null; // make sure "" is null

                // if we are missing a fragment or an identifier for the fragment, then I should just stop because there are no more rows in the file...
                if ($identifier == "" || ($newFragment == "" && $action != 'delete')) {
                    break;
                }

                $changeSet = $this->buildChangeSet($identifier, $newFragment, $row, $action, $discriminator);
                $changeSets->push($changeSet);
            }

            return true; // exit closure
        });

        return $changeSets;
    }

    /**
     * @param string $identifier
     * @param string $fragment
     * @param int $row
     * @param string $action
     * @param string $discriminator
     * @return ChangeSet
     */
    protected function buildChangeSet($identifier, $fragment, $row, $action = 'update', $discriminator = null)
    {
        $meta = $this->getDefaultFragmentTableInformation();
        $columns = $this->buildColumns($meta, $fragment, $identifier, $discriminator);
        $where = $this->buildWhere($meta, $identifier, $discriminator);
        switch ($action) {
            case 'update':
                $update = $this->buildUpdate($meta, $columns, $where);
                return new ChangeSet($this->argument('author'), $row, $update);
                break;
            case 'insert':
                $insert = $this->buildInsert($meta, $columns);
                return new ChangeSet($this->argument('author'), $row, $insert);
                break;
            case 'upsert':
                $conditionalInsert = $this->buildConditionalInsert($meta, $columns, $where);
                $update = $this->buildUpdate($meta, $columns, $where);
                return new ChangeSet($this->argument('author'), $row, $conditionalInsert, $update);
                break;
            case 'delete':
                $delete = $this->buildDelete($meta, $where);
                return new ChangeSet($this->argument('author'), $row, $delete);
                break;
            default:
                $update = $this->buildUpdate($meta, $columns, $where);
                return new ChangeSet($this->argument('author'), $row, $update);
                break;
        }
    }

    /**
     * @param array $meta
     * @param string $fragment
     * @param string $identifier
     * @param string $discriminator
     * @return array
     */
    protected function buildColumns($meta, $fragment, $identifier, $discriminator = null)
    {
        $updateColumns = $this->getUpdateColumns($meta, $fragment);

        $insertColumns = $this->getInsertColumns($meta, $fragment, $identifier, $discriminator);

        return compact('updateColumns', 'insertColumns');
    }

    /**
     * @param array $meta
     * @param string $identifier
     * @param string $discriminator
     * @return Where
     */
    protected function buildWhere($meta, $identifier, $discriminator = null)
    {
        // builds something like "FRAGMENT_RATING_CODE = 'ValueInColumn'"
        $whereClauses = collect([new WhereClause($meta['fragment-identifier'], $identifier)]);

        if ($discriminator)
            $whereClauses->push(new WhereClause($meta['discriminator'], $discriminator));

        return new Where(...$whereClauses);
    }

    /**
     * @param array $meta
     * @param string $fragment
     * @param string $identifier
     * @param string $discriminator
     * @return Collection|Column[]
     */
    protected function getInsertColumns($meta, $fragment, $identifier, $discriminator): Collection
    {
        $insertColumns = new Collection();

        foreach ($meta['insert-columns']['fragment'] as $columnMeta) {
            $insertColumns->push(new Column($columnMeta, $fragment));
        }

        foreach ($meta['insert-columns']['valueComputed'] as $key => $columnMeta) {
            $insertColumns->push(new Column($key, $columnMeta, 'valueComputed'));
        }

        if ($discriminator != null) {
            foreach ($meta['insert-columns']['discriminator'] as $columnMeta) {
                $insertColumns->push(new Column($columnMeta, $discriminator));
            }
        }

        foreach ($meta['insert-columns']['identifier'] as $columnMeta) {
            $insertColumns->push(new Column($columnMeta, $identifier));
        }
        return $insertColumns;
    }

    /**
     * @param array $meta
     * @param string $fragment
     * @return Collection|Column[]
     */
    protected function getUpdateColumns($meta, $fragment): Collection
    {
        $updateColumns = new Collection();

        foreach ($meta['update-columns'] as $columnMeta) {
            $updateColumns->push(new Column($columnMeta, $fragment));
        }
        return $updateColumns;
    }

    /**
     * @param array $meta
     * @param array $columns
     * @param Where $where
     * @return Update
     */
    protected function buildUpdate($meta, $columns, $where): Update
    {
        $update = new Update($meta['schema'], $meta['table'], $where, ... $columns['updateColumns']);
        return $update;
    }

    /**
     * @param array $meta
     * @param array $columns
     * @param Where $where
     * @return ConditionalInsert
     */
    protected function buildConditionalInsert($meta, $columns, $where)
    {
        $conditionalInsert = new ConditionalInsert($meta['schema'], $meta['table'], $where, ... $columns['insertColumns']);
        return $conditionalInsert;
    }

    /**
     * @param array $meta
     * @param array $columns
     * @return Insert
     */
    protected function buildInsert($meta, $columns)
    {
        $insert = new Insert($meta['schema'], $meta['table'], ... $columns['insertColumns']);
        return $insert;
    }

    /**
     * @param array $meta
     * @param Where $where
     * @return Delete
     */
    protected function buildDelete($meta, $where)
    {
        $delete = new Delete($meta['schema'], $meta['table'], $where);
        return $delete;
    }
}
