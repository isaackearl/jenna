<?php

namespace App\Liquibase\Data\XMLTag;


use App\Liquibase\ChangeSetContent;
use App\Liquibase\Data\Traits\HasDataColumns;

class Insert extends ChangeSetContent
{

    use HasDataColumns;

    /**
     * ChangeSet constructor.
     * @param string $schema
     * @param string $table
     * @param Column[] $columns
     */
    public function __construct($schema, $table, Column ... $columns)
    {
        parent::__construct($schema, $table);
        $this->columns = $columns;
    }

    /**
     * @return string;
     */
    function getChangeSetType()
    {
        return 'insert';
    }

    /**
     * @return string
     */
    protected function getStub()
    {
        return $this->files->get(app_path() . '/stubs/insert.stub');
    }

    /**
     * @param string $stub
     * @return string
     */
    protected function replaceValues(string $stub)
    {
        $stub = $this->replaceSchema($stub);
        $stub = $this->replaceTable($stub);
        return $this->replaceColumns($stub);
    }
}